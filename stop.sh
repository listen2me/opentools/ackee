#!/bin/bash
# *********************************************************
# * Author        : LEI Sen
# * Email         : sen.lei@outlook.com
# * Create time   : 2023-02-24 10:06
# * Last modified : 2023-02-24 10:07
# * Filename      : stop.sh
# * Description   : 
# *********************************************************

docker-compose -f docker-compose.yml down
